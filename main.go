package main

import (
	"fmt"
)

type List struct {
	Head   *Element
	Tail   *Element
	Length int
}

type Element struct {
	Value int
	Next  *Element
}

func (list *List) add(i int) {
	el := Element{Value: i}
	list.Length++

	if list.Head == nil {
		list.Head = &el
		list.Tail = list.Head
	} else {
		list.Tail.Next = &el
		list.Tail = list.Tail.Next
	}
}

func (list *List) print() {
	el := list.Head
	for el != nil {
		fmt.Println(el.Value)
		el = el.Next
	}
}

func (list *List) reverse() {
	h := list.Head

	x := h
	y := x.Next

	list.Tail = h
	list.Tail.Next = nil

	for y != nil {
		buf := y.Next
		y.Next = x
		x = y
		y = buf
	}

	list.Head = x

	fmt.Println("reverse")
}

func main() {
	var list List = List{
		Length: 0,
	}

	for i := 0; i < 10; i++ {
		list.add(i)
	}

	list.print()

	list.reverse()

	list.print()
}
